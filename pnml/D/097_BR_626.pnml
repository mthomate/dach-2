//============================================================
// ID097 BR 626
//============================================================

//
//PROPERTIES
//

item(FEAT_TRAINS, item_dmu_BR_626) {
    property {
        name:						string(STR_DMU_BR_626);
		introduction_date:			date(1981,9,9);
        model_life:					VEHICLE_NEVER_EXPIRES; //should extend 5 years after the intended end year
		retire_early:				1;
        vehicle_life:				40;
		reliability_decay:			5;
        climates_available: 		ALL_CLIMATES;

        engine_class: 				ENGINE_CLASS_DIESEL;
        visual_effect_and_powered: 	visual_effect_and_powered(VISUAL_EFFECT_DIESEL, -2, ENABLE_WAGON_POWER);
        length: 					8;
        dual_headed: 				0;

        speed: 						100 km/h;
        power: 						500 kW;
        extra_power_per_wagon: 		0;
        weight: 					46 ton;
        extra_weight_per_wagon:		0 ton;
        tractive_effort_coefficient:0.3;
        air_drag_coefficient: 		0.05;

		refittable_cargo_classes: 	bitmask(CC_PASSENGERS);
		default_cargo_type: 		PASS;	
        cargo_capacity: 			80;
        refit_cost: 				0;
        loading_speed:				60; //set via global setting
		
		ai_special_flag:			AI_FLAG_PASSENGER;
        misc_flags: 				bitmask(TRAIN_FLAG_NO_BREAKDOWN_SMOKE,TRAIN_FLAG_TILT);
        sprite_id: 					SPRITE_ID_NEW_TRAIN;
        bitmask_vehicle_info: 		0;
	}
}



//------------------------------------------------------------

//
// SPRITESETS
//

spriteset(spriteset_dmu_BR_626_purchase, 		"gfx/D/RAILCAR/NE81.png"){tmpl_purchase(0)}
spriteset(spriteset_dmu_BR_626_avg_vt,	 		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,20)}
spriteset(spriteset_dmu_BR_626_avg_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,50)}
spriteset(spriteset_dmu_BR_626_avg_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,80)}
spriteset(spriteset_dmu_BR_626_bob_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,110)}
spriteset(spriteset_dmu_BR_626_bob_vt_rev, 		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,140)}
spriteset(spriteset_dmu_BR_626_bob_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,170)}
spriteset(spriteset_dmu_BR_626_kvg_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_kvg_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_kvg_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_egp_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_egp_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_egp_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_weg1_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_weg1_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_weg1_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_weg2_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_weg2_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_weg2_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_sweg_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_sweg_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_sweg_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_hzl_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_hzl_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_hzl_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_db_vt,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_626_db_vt_rev,		"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,230)}
spriteset(spriteset_dmu_BR_626_db_vs,			"gfx/D/RAILCAR/NE81.png"){tmpl_32_short(0,200)}


//
// SPRITE SELECTION
//


switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_forward_vr,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_626_vr_A;
    spriteset_dmu_BR_626_vr_B;
}
switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_forward_ic,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_626_ic_A;
    spriteset_dmu_BR_626_ic_B;
}

switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_forward,cargo_subtype){
    0: 	sw_dmu_BR_626_sprites_forward_vr;
		sw_dmu_BR_626_sprites_forward_ic;
}

switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_backward_vr,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_626_vr_A_rev;
    spriteset_dmu_BR_626_vr_B_rev;
}
switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_backward_ic,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_626_ic_A_rev;
    spriteset_dmu_BR_626_ic_B_rev;
}


switch(FEAT_TRAINS,SELF, sw_dmu_BR_626_sprites_backward,[STORE_TEMP(num_vehs_in_consist - position_in_consist - 1 - position_in_consist, 0x10F), var[0x61, 0, 0x1F, 0xF2]]){
    0: 	sw_dmu_BR_626_sprites_backward_vr;
		sw_dmu_BR_626_sprites_backward_ic;
}

switch(FEAT_TRAINS,PARENT, sw_dmu_BR_626_sprites,vehicle_is_reversed){
    0: sw_dmu_BR_626_sprites_forward;
	   sw_dmu_BR_626_sprites_backward;
}

//Purchase menu sprite
switch(FEAT_TRAINS, SELF, sw_dmu_BR_626_purchase,1==1){
	spriteset_dmu_BR_626_purchase;
}


switch(FEAT_TRAINS,SELF,sw_dmu_BR_626_cargo_subtype_text_r_i,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_ROT);
    1: return string(STR_CARGO_SUBTYPE_IC);
       return CB_RESULT_NO_TEXT;
}
switch(FEAT_TRAINS,SELF,sw_dmu_BR_626_cargo_subtype_text_r,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_ROT);
       return CB_RESULT_NO_TEXT;
}


switch(FEAT_TRAINS,SELF,sw_dmu_BR_626_cargo_subtype_text,current_year){
    0..2003:    sw_dmu_BR_626_cargo_subtype_text_r;
                sw_dmu_BR_626_cargo_subtype_text_r_i;
}



//VISUAL EFFECT AND POWERED

switch(FEAT_TRAINS, SELF, sw_dmu_BR_626_VE_and_powered_forward, position_in_vehid_chain % 2) {
     1: return visual_effect_and_powered(VISUAL_EFFECT_DIESEL, 0, ENABLE_WAGON_POWER);
        return visual_effect_and_powered(VISUAL_EFFECT_DISABLE, 0, DISABLE_WAGON_POWER);
}
switch(FEAT_TRAINS, SELF, sw_dmu_BR_626_VE_and_powered_backward, position_in_vehid_chain % 2) {
     0: return visual_effect_and_powered(VISUAL_EFFECT_DIESEL, 0, ENABLE_WAGON_POWER);
        return visual_effect_and_powered(VISUAL_EFFECT_DISABLE, 0, DISABLE_WAGON_POWER);
}
switch(FEAT_TRAINS, PARENT, sw_dmu_BR_626_VE_and_powered, vehicle_is_reversed) {
     0: sw_dmu_BR_626_VE_and_powered_forward;
        sw_dmu_BR_626_VE_and_powered_backward;
}

//------------------------------------------------------------


//
//ITEM GRAPHICS
//

item(FEAT_TRAINS, item_dmu_BR_626) {
    graphics {
    additional_text: 			return(string(STR_PURCHASE_MU_WITH_LIVERIES_DESC,string(STR_PURCHASE_TYPE_DMU),string(STR_DMU_BR_626_USAGE),string(STR_DMU_BR_626_EOS),string(STR_DMU_BR_626_LIVERIES),string(STR_DMU_BR_626_DESC)));
	cargo_subtype_text:			sw_dmu_BR_626_cargo_subtype_text;
	cargo_age_period:			sw_cargo_age_period_local;
	loading_speed:				sw_loading_speed_local;
    purchase: 					sw_dmu_BR_626_purchase;
	purchase_running_cost_factor:sw_dmu_BR_626_purchase_running_cost;
	running_cost_factor:		sw_dmu_BR_626_running_cost;
	visual_effect_and_powered: 	sw_dmu_BR_626_VE_and_powered;
    default:					sw_dmu_BR_626_sprites;
    }
}