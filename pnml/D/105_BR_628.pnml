//============================================================
// ID105 BR 628
//============================================================

//
//PROPERTIES
//

item(FEAT_TRAINS, item_dmu_BR_628) {
    property {
        name:						string(STR_DMU_BR_628);
				introduction_date:			date(1996,9,9);
        model_life:					10; //should extend 5 years after the intended end year
				retire_early:				1;
        vehicle_life:				40;
				reliability_decay:			5;
        climates_available: 		ALL_CLIMATES;

        engine_class: 				ENGINE_CLASS_DIESEL;
        visual_effect_and_powered: 	visual_effect_and_powered(VISUAL_EFFECT_DIESEL, -2, ENABLE_WAGON_POWER);
        length: 					8;
        dual_headed: 				0;

        speed: 						120 km/h;
        power: 						1080 kW;
        extra_power_per_wagon: 		0;
        weight: 					58 ton;
        extra_weight_per_wagon:		0 ton;
        tractive_effort_coefficient:0.3;
        air_drag_coefficient: 		0.05;

		refittable_cargo_classes: 	bitmask(CC_PASSENGERS);
		default_cargo_type: 		PASS;	
        cargo_capacity: 			72;
        refit_cost: 				0;
        loading_speed:				60; //set via global setting
		
		ai_special_flag:			AI_FLAG_PASSENGER;
        misc_flags: 				bitmask(TRAIN_FLAG_NO_BREAKDOWN_SMOKE,TRAIN_FLAG_TILT);
        sprite_id: 					SPRITE_ID_NEW_TRAIN;
        bitmask_vehicle_info: 		0;
	}
}



//------------------------------------------------------------

//
// SPRITESETS
//

spriteset(spriteset_dmu_BR_628_vr_purchase, 	"gfx/D/DMU/BR628.png"){tmpl_purchase_horizontal(0)}
spriteset(spriteset_dmu_BR_628_mint_purchase, 	"gfx/D/DMU/BR628.png"){tmpl_purchase_horizontal(119)}
spriteset(spriteset_dmu_BR_628_vr_A,	 		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,20)}
spriteset(spriteset_dmu_BR_628_vr_A_rev,		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,80)}
spriteset(spriteset_dmu_BR_628_vr_B,			"gfx/D/DMU/BR628.png"){tmpl_32_short(0,50)}
spriteset(spriteset_dmu_BR_628_vr_B_rev,		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,110)}
spriteset(spriteset_dmu_BR_628_mint_A,	 		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,140)}
spriteset(spriteset_dmu_BR_628_mint_A_rev,		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,200)}
spriteset(spriteset_dmu_BR_628_mint_B,			"gfx/D/DMU/BR628.png"){tmpl_32_short(0,170)}
spriteset(spriteset_dmu_BR_628_mint_B_rev,		"gfx/D/DMU/BR628.png"){tmpl_32_short(0,230)}


//
// SPRITE SELECTION
//


switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_forward_vr,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_628_vr_A;
    spriteset_dmu_BR_628_vr_B;
}
switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_forward_mint,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_628_mint_A;
    spriteset_dmu_BR_628_mint_B;
}
switch(FEAT_TRAINS,PARENT, sw_dmu_BR_628_sprites_forward_automatic,date_of_last_service){ //year_of_last_service
    0..date(1998,10,10): 	sw_dmu_BR_628_sprites_forward_mint;
							sw_dmu_BR_628_sprites_forward_vr;
}

switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_forward,cargo_subtype){
    0: 	sw_dmu_BR_628_sprites_forward_automatic;
    1: 	sw_dmu_BR_628_sprites_forward_mint;
		sw_dmu_BR_628_sprites_forward_vr;
}

switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_backward_vr,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_628_vr_A_rev;
    spriteset_dmu_BR_628_vr_B_rev;
}
switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_backward_mint,position_in_vehid_chain % 2){
    0: spriteset_dmu_BR_628_mint_A_rev;
    spriteset_dmu_BR_628_mint_B_rev;
}
switch(FEAT_TRAINS,PARENT, sw_dmu_BR_628_sprites_backward_automatic,date_of_last_service){ //year_of_last_service
    0..date(1998,10,10): 	sw_dmu_BR_628_sprites_backward_mint;
							sw_dmu_BR_628_sprites_backward_vr;
}

switch(FEAT_TRAINS,SELF, sw_dmu_BR_628_sprites_backward,[STORE_TEMP(num_vehs_in_consist - position_in_consist - 1 - position_in_consist, 0x10F), var[0x61, 0, 0x1F, 0xF2]]){
    0: 	sw_dmu_BR_628_sprites_backward_automatic;
    1: 	sw_dmu_BR_628_sprites_backward_mint;
		sw_dmu_BR_628_sprites_backward_vr;
}

switch(FEAT_TRAINS,PARENT, sw_dmu_BR_628_sprites,vehicle_is_reversed){
    0: sw_dmu_BR_628_sprites_forward;
	   sw_dmu_BR_628_sprites_backward;
}

//Purchase menu sprite
switch(FEAT_TRAINS, SELF, sw_dmu_BR_628_purchase,current_year){
    0..1998:	spriteset_dmu_BR_628_mint_purchase;
				spriteset_dmu_BR_628_vr_purchase;
}


switch(FEAT_TRAINS,SELF,sw_dmu_BR_628_cargo_subtype_text_m_r,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_MINT);
    2: return string(STR_CARGO_SUBTYPE_ROT);
       return CB_RESULT_NO_TEXT;
}
switch(FEAT_TRAINS,SELF,sw_dmu_BR_628_cargo_subtype_text_m,cargo_subtype){
    0: return string(STR_CARGO_SUBTYPE_AUTOMATIC);
    1: return string(STR_CARGO_SUBTYPE_MINT);
       return CB_RESULT_NO_TEXT;
}


switch(FEAT_TRAINS,SELF,sw_dmu_BR_628_cargo_subtype_text,current_year){
    0..1998:    sw_dmu_BR_628_cargo_subtype_text_m;
                sw_dmu_BR_628_cargo_subtype_text_m_r;
}


//ARTICULATION

switch(FEAT_TRAINS, SELF, sw_dmu_BR_628_articulated, extra_callback_info1) {
    0..1: return item_dmu_BR_628;
          return CB_RESULT_NO_MORE_ARTICULATED_PARTS;
}

//CAN ATTACH WAGON

switch(FEAT_TRAINS,SELF,sw_dmu_BR_628_can_attach_wagon2,vehicle_type_id){
    item_dmu_BR_628:return CB_RESULT_ATTACH_ALLOW;
    				return(string(STR_ATTACH_BR_628_ONLY));
}

switch(FEAT_TRAINS,SELF,sw_dmu_BR_628_can_attach_wagon,grfid){
    str2number("\4D\46\47\01"):	sw_dmu_BR_628_can_attach_wagon2;
    return(string(STR_ATTACH_BR_628_ONLY));
}


switch(FEAT_TRAINS, PARENT, sw_dmu_BR_628_length, position_in_vehid_chain % 2) {
     0: 6;
        8;
}

//VISUAL EFFECT AND POWERED

switch(FEAT_TRAINS, SELF, sw_dmu_BR_628_VE_and_powered_forward, position_in_vehid_chain % 2) {
     0: return visual_effect_and_powered(VISUAL_EFFECT_DIESEL, 0, ENABLE_WAGON_POWER);
        return visual_effect_and_powered(VISUAL_EFFECT_DISABLE, 0, DISABLE_WAGON_POWER);
}
switch(FEAT_TRAINS, SELF, sw_dmu_BR_628_VE_and_powered_backward, position_in_vehid_chain % 2) {
     1: return visual_effect_and_powered(VISUAL_EFFECT_DIESEL, 0, ENABLE_WAGON_POWER);
        return visual_effect_and_powered(VISUAL_EFFECT_DISABLE, 0, DISABLE_WAGON_POWER);
}
switch(FEAT_TRAINS, PARENT, sw_dmu_BR_628_VE_and_powered, vehicle_is_reversed) {
     0: sw_dmu_BR_628_VE_and_powered_forward;
        sw_dmu_BR_628_VE_and_powered_backward;
}

//------------------------------------------------------------


//
//ITEM GRAPHICS
//

item(FEAT_TRAINS, item_dmu_BR_628) {
    graphics {
    additional_text: 			return(string(STR_PURCHASE_MU_WITH_LIVERIES_DESC,string(STR_PURCHASE_TYPE_DMU),string(STR_DMU_BR_628_USAGE),string(STR_DMU_BR_628_EOS),string(STR_DMU_BR_628_LIVERIES),string(STR_DMU_BR_628_DESC)));
	articulated_part:			sw_dmu_BR_628_articulated;
	cargo_subtype_text:			sw_dmu_BR_628_cargo_subtype_text;
	can_attach_wagon:			sw_dmu_BR_628_can_attach_wagon;
	cargo_age_period:			sw_cargo_age_period_local;
	loading_speed:				sw_loading_speed_local;
    purchase: 					sw_dmu_BR_628_purchase;
	purchase_running_cost_factor:sw_dmu_BR_628_purchase_running_cost;
	running_cost_factor:		sw_dmu_BR_628_running_cost;
	visual_effect_and_powered: 	sw_dmu_BR_628_VE_and_powered;
    default:					sw_dmu_BR_628_sprites;
	length:						sw_dmu_BR_628_length;
    }
}